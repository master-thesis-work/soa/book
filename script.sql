create table book
(
    id           serial
        constraint book_pk
            primary key,
    title        varchar,
    quantity     integer default 0 not null,
    isbn         varchar,
    release_date timestamp
);

alter table book
    owner to postgres;

create unique index book_id_uindex
    on book (id);

create unique index book_isbn_uindex
    on book (isbn);

-- auto-generated definition
create table author_book
(
    id        serial
        constraint author_book_pk
            primary key,
    book_id   integer,
    author_id integer
);

alter table author_book
    owner to postgres;

create unique index author_book_id_uindex
    on author_book (id);

