package main

import (
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"library/internal/database"
	"library/internal/event"
	"library/internal/http"
	"log"
)

func main() {
	db := database.NewDb("pgx", fmt.Sprintf("postgres://postgres:postgres@localhost:5432/book?sslmode=disable"))

	err := db.Connect()
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		err = db.Close()
		if err != nil {
			log.Fatal(err)
		}
	}()

	eventer := event.NewNatsClient("nats://localhost:4222", []nats.Option{nats.MaxReconnects(-1)}...)

	err = eventer.Connect()
	if err != nil {
		log.Fatalf("[ERROR] error connecting to Nats.io - %s", err.Error())
	}

	defer eventer.Close(context.Background())

	server := http.NewServer(db, eventer)

	server.Run()
}
