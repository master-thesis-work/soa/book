package models

import "time"

type Book struct {
	ID          int       `json:"id" db:"id"`
	Quantity    int       `json:"quantity" db:"quantity"`
	Title       string    `json:"title" db:"title"`
	ISBN        string    `json:"isbn" db:"isbn"`
	ReleaseDate time.Time `json:"release_date" db:"release_date"`
	Genre       []*Genre  `json:"genre"`
}

type AddBookRequest struct {
	Book
	GenreID  int `json:"genre_id"`
	AuthorID int `json:"author_id"`
}

type Genre struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
