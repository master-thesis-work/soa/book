package models

type Member struct {
	IsActive    bool   `json:"is_active" db:"is_active"`
	ID          int    `json:"id" db:"id"`
	FirstName   string `json:"first_name" db:"first_name"`
	LastName    string `json:"last_name" db:"last_name"`
	PhoneNumber string `json:"phone_number" db:"phone_number"`
	Email       string `json:"email" db:"email"`
}

type MemberBooks struct {
	*Member
	Books []*Book `json:"books"`
}

type BorrowBookRequest struct {
	MemberID int `json:"member_id"`
	BookID   int `json:"book_id"`
}
