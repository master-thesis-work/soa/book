package models

type CustomResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}
