package event

import (
	"context"
	"encoding/json"
	"errors"
	"github.com/nats-io/nats.go"
	"log"
	"time"
)

type Handler func(ctx context.Context, event *Event) error

type Eventer interface {
	Connect() error
	Register(handlers map[string]Handler) (err error)
	Subscribe(ctx context.Context, subject string, done chan bool, handler Handler) error
	SubscribeQueue(ctx context.Context, subject, queue string, done chan bool, handler Handler) error
	Publish(ctx context.Context, subject string, message *Event) error
	Close(ctx context.Context) error
}

type Event struct {
	Source    string    `json:"source"`
	Version   string    `json:"version"`
	Message   string    `json:"message"`
	Subject   string    `json:"subject"`
	EventTime time.Time `json:"event_time"`
	Payload   []byte    `json:"payload"`
}

func (e *Event) Read() (int, []byte) {
	return 0, e.Payload
}

type Options struct {
	MaxReconnectAttempts int
	ReconnectWait        time.Duration
}

func DefaultOptions() *Options {
	return &Options{
		MaxReconnectAttempts: -1,
		ReconnectWait:        time.Second,
	}
}

type NatsClient struct {
	address string
	conn    *nats.Conn
	options []nats.Option
}

func NewNatsClient(addr string, options ...nats.Option) *NatsClient {
	return &NatsClient{
		address: addr,
		options: options,
	}
}

func (n *NatsClient) Connect() error {
	conn, err := nats.Connect(n.address, n.options...)
	if err != nil {
		return err
	}

	n.conn = conn

	return nil
}

func (n *NatsClient) Register(handlers map[string]Handler) (err error) {
	for event, handler := range handlers {
		ctx := context.TODO()
		if err := n.Subscribe(ctx, event, make(chan bool), handler); err != nil {
			return err
		}
	}

	return nil
}

func (n *NatsClient) Subscribe(ctx context.Context, subject string, _ chan bool, handler Handler) error {
	if n.conn == nil {
		return errors.New("there is no connection to server")
	}

	newHandler := func(msg *nats.Msg) {
		var event Event

		if err := json.Unmarshal(msg.Data, &event); err != nil {
			log.Println("[ERROR] error marshaling event struct")
		}

		event.Subject = msg.Subject

		if err := handler(ctx, &event); err != nil {
			log.Printf("[ERROR] on call handler: %s", err.Error())
		}
	}

	if _, err := n.conn.Subscribe(subject, newHandler); err != nil {
		return err
	}

	return nil
}

func (n *NatsClient) SubscribeQueue(ctx context.Context, subject, queue string, _ chan bool, handler Handler) error {
	if n.conn == nil {
		return errors.New("there is no connection to server")
	}

	newHandler := func(msg *nats.Msg) {
		var event Event
		err := json.Unmarshal(msg.Data, &event)

		if err != nil {
			log.Println("[ERROR] error marshaling event struct")
		}

		event.Subject = msg.Subject

		if err = handler(ctx, &event); err != nil {
			log.Printf("[ERROR] on call handler: %s", err.Error())
		}
	}

	if _, err := n.conn.QueueSubscribe(subject, queue, newHandler); err != nil {
		return err
	}

	return nil
}

func (n *NatsClient) Publish(_ context.Context, subject string, message *Event) error {
	if n.conn == nil {
		return errors.New("there is no connection to server")
	}

	js, err := json.Marshal(message)
	if err != nil {
		return err
	}

	return n.conn.Publish(subject, js)
}

func (n *NatsClient) Close(_ context.Context) error {
	if n.conn == nil {
		return errors.New("there is no connection to server")
	}

	return n.conn.Drain()
}
