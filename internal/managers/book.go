package managers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"library/internal/database"
	"library/internal/event"
	"library/pkg/models"
	"net/http"
	"time"
)

type Booker interface {
	UpsertBook(book *models.AddBookRequest) error
	Book(id int, isbn string) (*models.Book, error)
	Books(page, limit int) ([]*models.Book, error)
	DeleteBook(id int) error
	BorrowBook(id int) error
}

type Book struct {
	bookRepository database.Booker
	eventer        event.Eventer
}

func NewBook(bookRepository database.Booker, eventer event.Eventer) Booker {
	return &Book{
		bookRepository: bookRepository,
		eventer:        eventer,
	}
}

func (b *Book) BorrowBook(id int) error {
	return b.bookRepository.BorrowBook(id)
}

func (b *Book) UpsertBook(book *models.AddBookRequest) error {
	id, err := b.bookRepository.UpsertBook(&book.Book)
	if err != nil {
		return err
	}

	err = b.bookRepository.AddGenreBook(book.GenreID, id)
	if err != nil {
		return err
	}

	return b.bookRepository.AddAuthorBook(book.AuthorID, id)
}

func (b *Book) Book(id int, isbn string) (*models.Book, error) {
	ids, err := b.bookRepository.GenreBook(id)
	if err != nil {
		return nil, err
	}

	genre := make([]*models.Genre, 0)

	for _, value := range ids {
		g, err := request(value)
		if err != nil {
			return nil, err
		}

		genre = append(genre, g)
	}

	bb, err := b.bookRepository.Book(id, isbn)
	if err != nil {
		return nil, err
	}

	bb.Genre = genre

	return bb, nil
}

func (b *Book) Books(page, limit int) ([]*models.Book, error) {
	return b.bookRepository.Books(page, limit)
}

func (b *Book) DeleteBook(id int) error {
	err := b.bookRepository.DeleteBook(id)
	if err != nil {
		return err
	}

	return b.notifyOnBook(context.Background(), "book.update", id)
}

func (b *Book) notifyOnBook(ctx context.Context, eve string, id int) error {
	body := &event.Event{
		Source:    "book",
		Version:   "v1",
		Message:   "book update",
		Subject:   eve,
		EventTime: time.Now().UTC(),
	}

	eventMessage := struct {
		ID int
	}{
		ID: id,
	}

	js, err := json.Marshal(eventMessage)
	if err != nil {
		return err
	}

	body.Payload = js

	err = b.eventer.Publish(ctx, eve, body)
	if err != nil {
		return err
	}

	return nil
}

func request(id int) (*models.Genre, error) {
	url := fmt.Sprintf("http://localhost:8080/v1/gateway/genre?id=%d", id)

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}

	if res.StatusCode != http.StatusOK {
		return nil, errors.New("error")
	}

	defer res.Body.Close()

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, err
	}

	genre := new(models.Genre)

	err = json.Unmarshal(body, genre)
	if err != nil {
		return nil, err
	}

	return genre, nil
}
