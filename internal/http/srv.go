package http

import (
	healthCheck "github.com/RaMin0/gin-health-check"
	"github.com/gin-gonic/gin"
	"library/internal/database"
	"library/internal/event"
	"library/internal/managers"
	"log"
)

type server struct {
	router  *gin.Engine
	eventer event.Eventer
	db      database.DataStore
}

func NewServer(db database.DataStore, eventer event.Eventer) *server {
	return &server{
		router:  gin.New(),
		eventer: eventer,
		db:      db,
	}
}

func (srv *server) setupRouter() {
	srv.router.Use(gin.Recovery())
	srv.router.Use(healthCheck.Default())

	v1 := srv.router.Group("/v1")

	bookManager := managers.NewBook(srv.db.BookRepository(), srv.eventer)
	NewBook(bookManager).Init(v1)
}

func (srv *server) Run() {
	srv.setupRouter()

	if err := srv.router.Run(":8080"); err != nil {
		log.Fatal(err)
	}
}
