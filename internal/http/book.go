package http

import (
	"github.com/gin-gonic/gin"
	"library/internal/managers"
	"library/pkg/models"
	"net/http"
	"strconv"
)

type book struct {
	bookManager managers.Booker
}

func NewBook(bookManager managers.Booker) *book {
	return &book{
		bookManager: bookManager,
	}
}

func (b *book) Init(router *gin.RouterGroup) {
	route := router.Group("/book")

	route.GET("/books", b.books)
	route.POST("", b.createBook)
	route.POST("/borrow/:id", b.borrowBook)
	route.GET("", b.book)
	route.DELETE("/:id", b.deleteBook)
}

func (b *book) borrowBook(ctx *gin.Context) {
	idParam := ctx.Param("id")

	id, err := strconv.Atoi(idParam)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	err = b.bookManager.BorrowBook(id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}

func (b *book) books(ctx *gin.Context) {
	page := ctx.Query("page")
	limit := ctx.Query("limit")

	response, err := b.bookManager.Books(models.HandlePagination(limit, page))
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	if len(response) == 0 {
		ctx.JSON(http.StatusNotFound, models.CustomResponse{
			Code:    http.StatusNotFound,
			Message: http.StatusText(http.StatusNotFound),
		})

		return
	}

	ctx.JSON(http.StatusOK, response)
}

func (b *book) createBook(ctx *gin.Context) {
	requestBody := new(models.AddBookRequest)

	if err := ctx.BindJSON(requestBody); err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	err := b.bookManager.UpsertBook(requestBody)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}

func (b *book) book(ctx *gin.Context) {
	idParam := ctx.Query("id")
	isbn := ctx.Query("isbn")

	if idParam == "" && isbn == "" {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	var id int

	var err error

	if idParam != "" {
		id, err = strconv.Atoi(idParam)
		if err != nil {
			ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

			return
		}
	}

	response, err := b.bookManager.Book(id, isbn)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, response)
}

func (b *book) deleteBook(ctx *gin.Context) {
	idParam := ctx.Param("id")

	id, err := strconv.Atoi(idParam)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, models.CustomResponse{Code: http.StatusBadRequest, Message: http.StatusText(http.StatusBadRequest)})

		return
	}

	err = b.bookManager.DeleteBook(id)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, models.CustomResponse{
			Code:    http.StatusInternalServerError,
			Message: err.Error(),
		})

		return
	}

	ctx.JSON(http.StatusOK, http.StatusText(http.StatusOK))
}
