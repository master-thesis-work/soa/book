package database

import (
	"context"
	"errors"
	"github.com/jmoiron/sqlx"
	"library/pkg/models"
)

type Booker interface {
	UpsertBook(book *models.Book) (int, error)
	Book(id int, isbn string) (*models.Book, error)
	Books(page, limit int) ([]*models.Book, error)
	AddAuthorBook(authorID int, bookID int) error
	AddGenreBook(genreID int, bookID int) error
	GenreBook(id int) ([]int, error)
	DeleteBook(id int) error
	BorrowBook(id int) error
}

type Book struct {
	db *sqlx.DB
}

func NewBook(db *sqlx.DB) Booker {
	return &Book{
		db: db,
	}
}

func (b *Book) AddGenreBook(genreID int, bookID int) error {
	query := `INSERT INTO book_genre(genre_id, book_id) VALUES (:genre_id, :book_id)`

	args := map[string]interface{}{
		"genre_id": genreID,
		"book_id":  bookID,
	}

	_, err := b.db.NamedExec(query, args)
	if err != nil {
		return err
	}

	return nil
}

func (b *Book) GenreBook(id int) ([]int, error) {
	row, err := b.db.Queryx(`SELECT genre_id FROM book_genre where book_id=$1`, id)
	if err != nil {
		return []int{}, err
	}

	genres := make([]int, 0)

	for row.Next() {
		var genreID int

		row.Scan(&genreID)

		genres = append(genres, genreID)
	}

	return genres, nil
}

func (b *Book) BorrowBook(id int) error {
	result, err := b.db.Exec(`UPDATE book SET quantity = quantity-1 WHERE id=$1 and quantity>0`, id)
	if err != nil {
		return err
	}

	count, err := result.RowsAffected()
	if err != nil {
		return err
	}

	if count == 0 {
		return errors.New("books not modified")
	}

	return nil
}

func (b *Book) UpsertBook(book *models.Book) (int, error) {
	query := `INSERT INTO book(title, quantity, isbn, release_date) VALUES ($1, $2, $3, $4)
			  ON CONFLICT(isbn) DO UPDATE SET title = excluded.title, quantity = excluded.quantity, release_date = excluded.release_date RETURNING id`

	var id int

	err := b.db.QueryRow(query, book.Title, book.Quantity, book.ISBN, book.ReleaseDate).Scan(&id)
	if err != nil {
		return 0, err
	}

	return id, nil
}

func (b *Book) Book(id int, isbn string) (*models.Book, error) {
	book := new(models.Book)

	err := b.db.QueryRowx(`SELECT * FROM book WHERE id = $1 or isbn = $2`, id, isbn).StructScan(book)
	if err != nil {
		return nil, err
	}

	return book, nil
}

func (b *Book) Books(page, limit int) ([]*models.Book, error) {
	books := make([]*models.Book, 0)

	row, err := b.db.Queryx(`SELECT * FROM book ORDER BY ID DESC LIMIT $1 OFFSET $2`, limit, page)
	if err != nil {
		return nil, err
	}

	for row.Next() {
		var book models.Book

		err = row.StructScan(&book)
		if err != nil {
			return nil, err
		}

		books = append(books, &book)
	}

	return books, nil
}

func (b *Book) DeleteBook(id int) error {
	tx, err := b.db.BeginTxx(context.Background(), nil)
	if err != nil {
		return err
	}

	tx.MustExec("DELETE FROM book WHERE id = $1", id)

	err = tx.Commit()
	if err != nil {
		e := tx.Rollback()
		if err != nil {
			return e
		}

		return err
	}

	return nil
}

func (b *Book) AddAuthorBook(authorID int, bookID int) error {
	query := `INSERT INTO author_book(author_id, book_id) VALUES (:author_id, :book_id)`

	args := map[string]interface{}{
		"author_id": authorID,
		"book_id":   bookID,
	}

	_, err := b.db.NamedExec(query, args)
	if err != nil {
		return err
	}

	return nil
}
